package cli

import (
    "fmt"
    "strings"
    "gitlab.com/mrvik/jdrive-client/socket"
)

type headerStructure struct{
    Workers, Downloads, Uploads int
}

func FormatAll(structure socket.TransactionData){
    str:=headerStructure{structure.Workers,len(structure.Downloads),len(structure.Uploads)};
    header(structure.Id,structure.SocketName, str);
    max:=getMaxPaths(structure);
    format:=fmt.Sprintf("|%%-4d|%%-%dq|%%-10s|%%-7s|\n", max);
    header:=tableHeaderCaption(format);
    if len(structure.Uploads) >0{
        tableHeader("Uploads", header, formatTransactionFile(structure.Uploads, format), max);
    }
    if len(structure.Downloads) >0{
        tableHeader("Downloads", header, formatTransactionFile(structure.Downloads, format), max);
    }
}

func header(p1, p2 string, st headerStructure){
    var name string;
    if p1 != "" {
        name=p1
    }else{
        name=p2
    }
    fmt.Printf("JDrive ID: %s with %d Workers\n",name, st.Workers);

    fmt.Printf("%-3d Uploads\n", st.Uploads);
    fmt.Printf("%-3d Downloads\n", st.Downloads);
    fmt.Print("\n");
}
func tableHeader(t string, header, inside string, max int){
    fmt.Printf("%s\n",t);
    formatPath:=strings.Repeat("-", max);
    format:=fmt.Sprintf("+----+%s+----------+-------+\n",formatPath);
    fmt.Print(format);
    fmt.Print(header);
    fmt.Print(format);
    fmt.Print(inside);
    fmt.Print(format);
}

func tableHeaderCaption(format string) string{
    return fmt.Sprintf(strings.ReplaceAll(format, "d", "s"), "ID", "File path", "Event type", "Started");
}

func formatTransactionFile(array []socket.TransactionFile, format string) string{
    var str string;
    for k,v := range array {
        str+=fmt.Sprintf(format,k,v.Path, v.Type, yesNo(v.Working));
    }
    return str;
}

func yesNo(b bool) string{
    if b {
        return "yes";
    }
    return "no"
}

func getMaxPaths(transaction socket.TransactionData) int{
    max:=10;
    for _,v := range append(transaction.Uploads,transaction.Downloads...) {
        max=maxInt(max,len(v.Path));
    }
    return max+2;
}

func maxInt(a,b int) int {
    if a>b {
        return a;
    }
    return b;
}
