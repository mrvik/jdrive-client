package main

import (
    "fmt"
    "io/ioutil"
    "os"
    "strings"
    "gitlab.com/mrvik/jdrive-client/socket"
    "gitlab.com/mrvik/jdrive-client/cli"
)

const version="1.0";

type Config struct{
    sockets []string // Array of sockets to do the action
}

func main(){
    var args []string;
    if len(os.Args) < 1 {
        args=make([]string,0);
    }else{
        args = os.Args[1:];
    }
    config := parseArguments(&args);
    info:=socket.GetAllInfoFromSockets(&config.sockets);
    for _,v := range info {
        cli.FormatAll(v);
    }
}

func parseArguments(args *[]string) Config{
    help:=[]string{"-h", "--help"};
    var config Config;
    if len(*args) < 1 {
        getAllSockets(&config);
    } else {
        switch argument := (*args)[0]; true{
            case isInStringArray(&argument, &help):{
                defer os.Exit(0);
                printHelp();
            }
            default:{
                config.sockets=(*args)[0:];
            }
        }
    }
    return config;
}

func isInStringArray(str *string, arr *[]string) bool {
    for _,v := range *arr {
        if v==*str {
            return true;
        }
    }
    return false;
}

func printHelp(){
    fmt.Print("Usage: jdrive-client [sockets...]\n\n");
    fmt.Printf("JDrive CLIent version %s\n", version);
    fmt.Printf("\nConnect to JDrive socket and retrive information\n");
}

func getAllSockets(config *Config){
    var out []string;
    search:=[2]string{"jdrive-", ".sock"};
    dir:="/tmp"; //As fallback dir
    if r:=os.Getenv("XDG_RUNTIME_DIR"); r!=""{
        dir=r;
    }
    files,err:=ioutil.ReadDir(dir);
    if err != nil {
        panic(err);
    }
    for _,file := range files {
        if name:=file.Name(); !file.IsDir()&&strings.HasPrefix(name, search[0])&&strings.HasSuffix(name, search[1]) {
            out=append(out, dir+"/"+name);
        }
    }
    config.sockets=out;
}
