package socket

import (
    "errors"
    "bytes"
    "fmt"
    "os"
    "time"
    "io"
    "encoding/json"
    "net"
)

type WelcomeID struct{
    Id string
}

type QueueBasic struct{
    Error bool
    Message string `json:"msg"`
}

type UploadsDownloads struct{
    Uploads []TransactionFile
    Downloads []TransactionFile
}

type QueueResult struct{
    QueueBasic
    Data UploadsDownloads
}

type TransactionFile struct{
    Type string
    Working bool
    Path string
}

type TransactionData struct{
    SocketName string
    Id string
    UploadsDownloads
    Workers int
}

type WorkerQuery struct{
    QueueBasic
    Data int
}

type SocketResult struct{
    Error error
    Result *TransactionData
}

func reader(r io.Reader, messages chan []byte){
    var buf []byte;
    for {
        /* When buffer comes full, starts eating bytes from start, so an invalid and incomplete message is left.
         * This way, each iteration reads 1024 bytes at most and sends themvia message channel
         */
        buf=make([]byte,1024);
        n,err:=r.Read(buf[:]);
        if err != nil {
            fmt.Fprintf(os.Stderr, "Read from socket failed: %s\n", err);
            return;
        }
        if n>0 {
            messages <- buf[:n];
        }
    }
}

func readAll(msgs *chan []byte)[]byte{
    buf:=bytes.NewBuffer(nil);
    for{
        select{
        case msg:=<-*msgs:
            buf.Write(msg);
            continue;
        default:
        }
        break;
    }
    return buf.Bytes();
}

func handshake(to string, messages *chan []byte) (*TransactionData,net.Conn,error){
    c,err:=net.Dial("unix", to);
    var transaction TransactionData;
    if err!=nil {
        return &transaction,c,err;
    }
    go reader(c,*messages);
    transaction.Id,err=getIDFromJSON(<-*messages);
    return &transaction,c,err;
}

func getAllData(path string, channel chan SocketResult){
    messages:=make(chan []byte, 10);
    var complete TransactionData;
    rt := SocketResult{nil,&complete};
    transaction,c,err:=handshake(path,&messages);
    if err != nil {
        checkAndSend(err, rt, channel);
        return;
    }
    complete.Id=(*transaction).Id;
    c.Write([]byte("all"));
    time.Sleep(500*time.Millisecond);
    complete.SocketName=path;
    rqueue,err:=getAllQueue(readAll(&messages));
    if rqueue.Error {
        checkAndSend(rqueue.Error, rt, channel);
        return;
    }
    if err != nil {
        checkAndSend(err, rt, channel);
        return;
    }
    c.Write([]byte("workers"));
    complete.Downloads=rqueue.Data.Downloads;
    complete.Uploads=rqueue.Data.Uploads;
    wquery,err:=getWorkers(<-messages);
    if wquery.Error {
        checkAndSend(wquery.Error, rt, channel);
        return;
    }
    complete.Workers=wquery.Data;
    channel<-rt;
}

func checkAndSend(value interface{}, rt SocketResult, channel chan SocketResult){
    switch err:=value.(type){
    case string:
        rt.Error=errors.New(err);
    case error:
        rt.Error=err;
    default:
        rt.Error=errors.New(fmt.Sprintf("Unkown error: %v with type %T", err, err));
    }
    channel<-rt;
}

func getWorkers(msg []byte) (WorkerQuery, error){
    var str WorkerQuery;
    err:=json.Unmarshal(msg, &str);
    var definedErr error;
    if err != nil {
        definedErr=errors.New(fmt.Sprintf("Error parsing received JSON: %s\n\tMessage: %s", err, string(msg)));
    }
    return str,definedErr;
}

func getAllQueue(msg []byte) (QueueResult, error){
    var str QueueResult
    err:=json.Unmarshal(msg, &str);
    var definedErr error;
    if err != nil {
        definedErr=errors.New(fmt.Sprintf("Error parsing received JSON: %s\n\tMessage: %s", err, string(msg)));
    }
    return str,definedErr;
}

func getIDFromJSON(welcomeMessage []byte) (string, error){
    var structure WelcomeID;
    err:=json.Unmarshal(welcomeMessage, &structure);
    var definedErr error;
    if err != nil {
        definedErr=errors.New(fmt.Sprintf("Error parsing received JSON: %s\n\tMessage: %s", err, string(welcomeMessage)));
    }
    return structure.Id,definedErr;
}

func GetAllInfoFromSockets(sockets *[]string) []TransactionData{
    channels:=make([]chan SocketResult,len(*sockets));
    dt:=make([]TransactionData,len(*sockets));
    for k,v := range *sockets {
        channels[k]=make(chan SocketResult);
        go getAllData(v,channels[k]);
    }
    for k,v := range channels{
        rt:=<-v;
        if rt.Error != nil {
            panic(rt.Error);
        }
        dt[k]=*rt.Result;
    }
    return dt;
}
